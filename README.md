# laravel_redis_support

#### 介绍
集成Reids


#### 使用说明

1. 引入
```
composer require ktnw/redis_support
```
2. 发布
```
php artisan vendor:publish --provider="Ktnw\RedisSupport\Providers\RedisSupportServiceProvider"
```
3. 具体使用请参数RedisUtils
